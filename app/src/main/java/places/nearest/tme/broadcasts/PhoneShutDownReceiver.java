package places.nearest.tme.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import places.nearest.tme.appbase.BaseApplication;
import places.nearest.tme.service.LocationService;

public class PhoneShutDownReceiver extends BroadcastReceiver {

    private static final String TAG = PhoneShutDownReceiver.class.getSimpleName();
    private static boolean firstShutdown = false;

    @Override
    public void onReceive(Context context, Intent intent) {

        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent, context);
        asyncTask.execute();
    }

    private static class Task extends AsyncTask {

        private final PendingResult pendingResult;
        private final Intent intent;
        private final Context context;

        private Task(PendingResult pendingResult, Intent intent, Context context) {
            this.pendingResult = pendingResult;
            this.intent = intent;
            this.context = context;
        }

        @Override
        protected Object doInBackground(Object[] objects) {


            if (intent.getAction().matches(Intent.ACTION_SHUTDOWN)
                    || intent.getAction().matches(Intent.ACTION_REBOOT)
            ) {

                Intent pushIntent = new Intent(context, LocationService.class);

                pushIntent.putExtra("power", "0");

                if (firstShutdown) {
                    firstShutdown = false;
                    setIntentValues(pushIntent);

                } else { //firstBoot false
                    firstShutdown = true;
                    setIntentValues(pushIntent);
                }

                return "";
            }
            return "";
        }

        private void setIntentValues(Intent pushIntent) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(BaseApplication.getApp(), pushIntent);
            } else {
                context.startService(pushIntent);
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // Must call finish() so the BroadcastReceiver can be recycled.
            pendingResult.finish();
        }

    }

}
