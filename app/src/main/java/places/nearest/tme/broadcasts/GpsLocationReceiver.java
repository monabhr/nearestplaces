package places.nearest.tme.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import places.nearest.tme.appbase.BaseApplication;
import places.nearest.tme.service.LocationService;

public class GpsLocationReceiver extends BroadcastReceiver {

    private static final String TAG = GpsLocationReceiver.class.getSimpleName();
    private static boolean firstConnect = true;


    @Override
    public void onReceive(Context context, Intent intent) {
        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent, context);
        asyncTask.execute();
    }

    private static class Task extends AsyncTask {

        private final PendingResult pendingResult;
        private final Intent intent;
        private final Context context;


        private Task(PendingResult pendingResult, Intent intent, Context context) {
            this.pendingResult = pendingResult;
            this.intent = intent;
            this.context = context;

        }

        @Override
        protected Object doInBackground(Object[] objects) {


            if (intent.getAction().matches(LocationManager.PROVIDERS_CHANGED_ACTION)) {

                Intent pushIntent = new Intent(context, LocationService.class);

                boolean gps_enabled = false;
                boolean network_enabled = false;

                LocationManager lm = (LocationManager) BaseApplication.getApp()
                        .getSystemService(Context.LOCATION_SERVICE);

                gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (firstConnect) {
                    firstConnect = false;
                    setIntentValues(pushIntent, gps_enabled, network_enabled);

                } else { //firstConnect false
                    firstConnect = true;
                    setIntentValues(pushIntent, gps_enabled, network_enabled);
                }

                return "";
            }

            return "";
        }

        private void setIntentValues(Intent pushIntent, boolean gps_enabled, boolean network_enabled) {
            if (gps_enabled || network_enabled) {
                pushIntent.putExtra("gps", "1");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(BaseApplication.getApp(), pushIntent);
                } else {
                    context.startService(pushIntent);
                }

            } else {
                pushIntent.putExtra("gps", "0");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(BaseApplication.getApp(), pushIntent);
                } else {
                    context.startService(pushIntent);
                }
            }


        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // Must call finish() so the BroadcastReceiver can be recycled.
            pendingResult.finish();
        }

    }

}
