package places.nearest.tme.broadcasts;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.ContextCompat;

import places.nearest.tme.appbase.BaseApplication;
import places.nearest.tme.service.LocationService;

public class NetworkCheckReceiver extends BroadcastReceiver {

    private static final String TAG = NetworkCheckReceiver.class.getSimpleName();
    private static Boolean isConnect = false;
    private static boolean firstConnect = false;


    @Override
    public void onReceive(Context context, Intent intent) {
        final PendingResult pendingResult = goAsync();
        Task asyncTask = new Task(pendingResult, intent, context);
        asyncTask.execute();
    }


    private static class Task extends AsyncTask {

        private final PendingResult pendingResult;
        private final Intent intent;
        private final Context context;


        private Task(PendingResult pendingResult, Intent intent, Context context) {
            this.pendingResult = pendingResult;
            this.intent = intent;
            this.context = context;
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            boolean Internet = checkInternet(context);
            if (Internet) {

                Intent pushIntent = new Intent(context, LocationService.class);


                if (firstConnect) {
                    firstConnect = false;
                    setIntentValues(pushIntent, Internet);

                } else { //firstConnect false
                    firstConnect = true;
                    setIntentValues(pushIntent, Internet);
                }

                return "";
            }

            return "";
        }

        private void setIntentValues(Intent pushIntent, boolean Internet) {
            if (Internet) {
                pushIntent.putExtra("net", "1");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(BaseApplication.getApp(), pushIntent);
                } else {
                    context.startService(pushIntent);
                }

            } else {
                pushIntent.putExtra("net", "0");

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    ContextCompat.startForegroundService(BaseApplication.getApp(), pushIntent);
                } else {
                    context.startService(pushIntent);
                }
            }


        }


        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            // Must call finish() so the BroadcastReceiver can be recycled.
            pendingResult.finish();
        }
    }

    static boolean checkInternet(Context context) {
        ServiceManager serviceManager = new ServiceManager(context);
        if (serviceManager.isNetworkAvailable()) {
            return true;
        } else {
            return false;
        }
    }
}
