package places.nearest.tme.service;

import android.location.Location;

import places.nearest.tme.localdb.NavigationEntity;

public class LocationPresenter implements LocationContract.locPresenter {

    LocationService service;
    LocationContract.locModel lmodel = new LocationModel();

    public LocationPresenter(LocationService service) {
        this.service = service;
        lmodel.onAttachPresenter(this);
    }

    @Override
    public void onSaveLocal(NavigationEntity nav) {
        lmodel.onSaveLocal(nav);
    }

    @Override
    public void onSaveLocalSucceed(long insertedId) {
        service.onSaveLocalSucceed(insertedId);
    }

    @Override
    public void onSaveLocalFailed(String message) {
        service.onSaveLocalFailed(message);
    }


    @Override
    public void onGetLastLocation(Location location) {
        lmodel.onGetLastLocation(location);
    }

    @Override
    public void onGetLastLocationResult(NavigationEntity result,Location location) {
        service.onGetLastLocationResult(result,location);
    }

}
