package places.nearest.tme.service;

import android.location.Location;

import com.orhanobut.logger.Logger;

import java.util.List;

import places.nearest.tme.localdb.DatabaseAdapter;
import places.nearest.tme.localdb.NavigationEntity;


public class LocationModel implements LocationContract.locModel {

    String TAG = LocationModel.class.getSimpleName();
    LocationContract.locPresenter presenter;
    DatabaseAdapter adapter = new DatabaseAdapter();

    @Override
    public void onAttachPresenter(LocationContract.locPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onSaveLocal(NavigationEntity model) {
        try {
            long insertedId = adapter.insertLocation(model);
            presenter.onSaveLocalSucceed(insertedId);
        } catch (Exception ex) {
            presenter.onSaveLocalFailed(ex.getLocalizedMessage());
        }
    }

    @Override
    public void onGetLastLocation(Location location) {
        try {
            List<NavigationEntity> lst = adapter.getAll();

            if (lst.size() > 0) {
                NavigationEntity ent = lst.get(lst.size() - 1);
                presenter.onGetLastLocationResult(ent,location);
            } else
                presenter.onGetLastLocationResult(null,location);
        } catch (Exception ex) {
            Logger.e(TAG, ex.getLocalizedMessage());
        }
    }
}
