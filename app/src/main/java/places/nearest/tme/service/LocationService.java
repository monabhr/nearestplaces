package places.nearest.tme.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.orhanobut.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import places.nearest.tme.R;
import places.nearest.tme.appbase.BaseApplication;
import places.nearest.tme.apputils.locUtils;
import places.nearest.tme.localdb.NavigationEntity;
import places.nearest.tme.localdb.PublicMethods;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static places.nearest.tme.apputils.Constants.ACTION_BROADCAST;
import static places.nearest.tme.apputils.Constants.EXTRA_LOCATION;

public class LocationService extends Service implements LocationContract.locService {

    String TAG = LocationService.class.getSimpleName();

    LocationContract.locPresenter presenter = new LocationPresenter(this);
    private LocationRequest locationrequest;
    //private LocationClient locationclient;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;

    NavigationEntity dbModel = new NavigationEntity();
    Location mLocation;

    private static int gps_intent_count = 1;
    private static int power_intent_count = 1;

    String gpsProviderState = "";
    String powerState = "";

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 8000;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 8000;
    private static final long MIN_DISPLACEMENT = 0;

    /**
     * The name of the channel for notifications.
     */
    private static final String CHANNEL_ID = "loc_channel_01";
    private static int Notif_ID = 1339;
    Handler mServiceHandler = new Handler();


    private boolean mChangingConfiguration = false;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder mBuilder;
    NotificationChannel notificationChannel;

    LocationRequest mLocationRequest;


    private final IBinder mBinder = new LocalBinder();

    //*********************************************************************************//
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        Logger.i(TAG, "in onBind()");
        mChangingConfiguration = false;
        return mBinder;
        //return null;
    }

    public class LocalBinder extends Binder {
        public LocationService getService() {
            return LocationService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        int resp = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (resp != ConnectionResult.SUCCESS) {
            PublicMethods.Companion.toast("Google Play Service Error " + resp);

        } else {
            mFusedLocationClient = getFusedLocationProviderClient(
                    this);

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    onNewLocation(locationResult.getLastLocation());
                }
            };
            createLocationRequest();
            getLastLocation();

            HandlerThread handlerThread = new HandlerThread(TAG);
            handlerThread.start();
            mServiceHandler = new Handler(handlerThread.getLooper());

            startServiceWithNotification();

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent.getExtras() != null)
            if (intent.hasExtra("gps") || intent.hasExtra("power") || intent.hasExtra("net")) {
                manageReceiverIntents(intent);
            }

        return super.onStartCommand(intent, flags, startId);
    }

    private void manageReceiverIntents(Intent intent) {
        try {
            if (intent.hasExtra("gps")) {
                if (gps_intent_count % 2 != 0) {
                    gpsProviderState = intent.getStringExtra("gps");
                    if (gpsProviderState.equals("1")) {
                        removeLocationUpdates();
                        requestLocationUpdates();
                    } else
                        removeLocationUpdates();

                }
                gps_intent_count += 1;
            }

            if (intent.hasExtra("net")) {
                if (intent.getStringExtra("net").equals("1")) {
                    removeLocationUpdates();
                    requestLocationUpdates();
                } else
                    removeLocationUpdates();
            }

            if (intent.hasExtra("power")) {
                if (power_intent_count % 2 != 0) {
                    powerState = intent.getStringExtra("power");
                    Logger.d("power intent " + powerState);
                    if (gpsProviderState.equals("1")) {
                        removeLocationUpdates();
                        requestLocationUpdates();
                    } else
                        removeLocationUpdates();
                }

                power_intent_count += 1;
            }

        } catch (Exception ex) {
            Logger.e(TAG, ex.getLocalizedMessage());
        }
    }


    private void startServiceWithNotification() {

        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        String Description = "location app is running";


        mBuilder = new NotificationCompat.Builder(this, null);

        mBuilder.setContentTitle("location")
                .setContentText("location is running")
                .setSmallIcon(R.mipmap.ic_luncher)
                .setPriority(Notification.PRIORITY_LOW)
                .setVibrate(null)
                .setSound(null, 0)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setOngoing(true)
                .setAutoCancel(false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            notificationChannel = new NotificationChannel(CHANNEL_ID,
                    "location is running",
                    NotificationManager.IMPORTANCE_LOW);

            notificationChannel.setShowBadge(false);
            notificationChannel.setDescription(Description);
            notificationChannel.enableVibration(false);

            mNotificationManager.createNotificationChannel(notificationChannel);
            mBuilder.setChannelId(CHANNEL_ID);

            this.startForeground(Notif_ID, mBuilder.build());

        } else {
            mBuilder.setChannelId(CHANNEL_ID);
            mNotificationManager.notify(Notif_ID, mBuilder.build());
            startService(new Intent(getBaseContext(), LocationService.class));
        }
    }


    public void requestLocationUpdates() {
        Logger.i(TAG, "Requesting location updates");
        locUtils.setRequestingLocationUpdates(this, true);

        Intent intent = new Intent(getApplicationContext(), LocationService.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(getBaseContext(), intent);
        } else {
            startService(intent);
        }

        try {

            //Looper.prepare();

            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback, Looper.getMainLooper());

        } catch (SecurityException unlikely) {
            locUtils.setRequestingLocationUpdates(this, false);
            Logger.e(TAG, "Lost location permission. Could not request updates. " + unlikely);
        }


    }

    public void removeLocationUpdates() {
        Logger.i(TAG, "Removing location updates");
        try {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            locUtils.setRequestingLocationUpdates(this, false);
            stopSelf();

        } catch (SecurityException unlikely) {
            // locUtils.setRequestingLocationUpdates(this, true);
            Logger.e(TAG, "Lost location permission. Could not remove updates. " + unlikely);
        }

    }

    private void getLastLocation() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLocation = task.getResult();
                        } else {
                            Logger.w(TAG, "Failed to get location.");
                        }
                    });
        } catch (SecurityException unlikely) {
            Logger.e(TAG, "Lost location permission." + unlikely);
        }
    }

    private void onNewLocation(Location location) {
        Logger.i(TAG, "New location: " + location);

        mLocation = location;

        if (mLocation.hasAccuracy()) {
            float accuracy = mLocation.getAccuracy();

            Logger.i("Accuracy " + accuracy);
            //if (mLocation.getAccuracy() > 15 ) { //15


            String msgT = String.format("newLocs :===== \r\n ( %s, %s)  ", mLocation.getLatitude(), mLocation.getLongitude());
            //PublicMethods.Companion.toast(msgT);
            Logger.i("onNewLocation  ===>>> " + msgT);

            presenter.onGetLastLocation(mLocation);


          /*  // Notify anyone listening for broadcasts about the new location.
            Intent intent = new Intent(ACTION_BROADCAST);
            intent.putExtra(EXTRA_LOCATION, location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

            setLocationValues(mLocation);*/
            //}
        }
        // Update notification content if running as a foreground service.
       /* if (serviceIsRunningInForeground(this)) {
            mNotificationManager.notify(Notif_ID, getNotification());
        }*/
    }


    private void setLocationValues(Location location) {
        try {
            dbModel.setAccuracy(location.getAccuracy());
            dbModel.setSpeed(location.getSpeed());

            Date date = new Date(location.getTime());

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
            String formattedDate = formatter.format(date);

            dbModel.setNavDate(formattedDate);
            dbModel.setAltitude(location.getAltitude());
            dbModel.setLatitude(location.getLatitude());
            dbModel.setLongitude(location.getLongitude());
            dbModel.setBearing(location.getBearing());


            String stringAddress = locUtils.getAddressFromCoordinates(BaseApplication.getApp(), location.getLatitude(), location.getLongitude());
            dbModel.setDescription(stringAddress);

            saveLocationOnDB();

        } catch (Exception ex) {
            Logger.e(TAG, ex.getLocalizedMessage());
        }
    }

    void saveLocationOnDB() {
        presenter.onSaveLocal(dbModel);
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setSmallestDisplacement(MIN_DISPLACEMENT);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public boolean serviceIsRunningInForeground(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(
                Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(
                Integer.MAX_VALUE)) {
            if (getClass().getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        System.gc();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            stopForeground(true); //true will remove notification
        }
        mServiceHandler.removeCallbacksAndMessages(null);
    }


    @Override
    public void onSaveLocalSucceed(long insertedId) {
        Logger.i(TAG, insertedId);
    }

    @Override
    public void onSaveLocalFailed(String message) {
        Logger.i(TAG, message);
    }

    /**
     * check if we have cashed location
     * and if that location has changed
     * and the distance is > 100
     */
    @Override
    public void onGetLastLocationResult(NavigationEntity result, Location location) {

        if (result != null) {
            if (mLocation != null) {

                Location loc1 = new Location("");
                loc1.setLatitude(result.getLatitude());
                loc1.setLongitude(result.getLongitude());

                float distanceInMeters = loc1.distanceTo(location);

                //if user location has changed
                if (distanceInMeters >= 100) {
                    saveDataSendIntent(mLocation);
                }
              /*  else {
                    Intent intent = new Intent(ACTION_BROADCAST);
                    intent.putExtra(EXTRA_LOCATION, loc1);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }*/
            }
        } else {
            saveDataSendIntent(mLocation);
        }


    }

    /**
     * sending intent to activity for refreshing the adapter
     * and save new location
     */
    private void saveDataSendIntent(Location mLocation) {
        Intent intent = new Intent(ACTION_BROADCAST);
        intent.putExtra(EXTRA_LOCATION, mLocation);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

        setLocationValues(mLocation);
    }
}
