package places.nearest.tme.service;

import android.location.Location;

import places.nearest.tme.localdb.NavigationEntity;


public interface LocationContract {

    interface locService {

        void onSaveLocalSucceed(long insertedId);
        void onSaveLocalFailed(String message);
        void onGetLastLocationResult(NavigationEntity result, Location location);

    }

    interface locPresenter {

        void onSaveLocal(NavigationEntity model);
        void onSaveLocalSucceed(long insertedId);
        void onSaveLocalFailed(String message);

        void onGetLastLocation(Location location);
        void onGetLastLocationResult(NavigationEntity result, Location location);


    }

    interface locModel {

        void onAttachPresenter(locPresenter presenter);
        void onSaveLocal(NavigationEntity model);
        void onGetLastLocation(Location location);


    }
}
