package places.nearest.tme.views.locationView;

import java.util.List;

import io.reactivex.disposables.Disposable;
import places.nearest.tme.localdb.NavigationEntity;
import places.nearest.tme.localdb.SquareEntity;

public interface LocationActContract {

    interface locView {

        void onGetNearestLocationSucceed(List<SquareEntity> model);
        void onGetNearestLocationFailed(String message);

        void onGetCashedSucceed(NavigationEntity ent);
        void onGetCashedFailed();
        void onGetAllDataResult(List<SquareEntity> model);
        void onGetAllDataFailed();

        void onDispose(Disposable d);

    }

    interface locPresenter {

        void onGetNearestLocations(String loc);
        void onGetNearestLocationSucceed(List<SquareEntity> model);
        void onGetNearestLocationFailed(String message);

        void onGetCashedLocation();
        void onGetCashedSucceed(NavigationEntity ent);
        void onGetCashedFailed();

        void onGetAllData(NavigationEntity ent);
        void onGetAllDataResult(List<SquareEntity> model);
        void onGetAllDataFailed();

        void onDispose(Disposable d);

    }

    interface locModel {
        void onAttachPresenter(locPresenter presenter);
        void onGetNearestLocations(String loc);

        void onGetCashedLocation();
        void onGetAllData(NavigationEntity ent);

    }

}
