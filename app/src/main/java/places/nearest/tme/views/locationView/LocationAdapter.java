package places.nearest.tme.views.locationView;/*
package cafe.bazar.baharlou.views.locationView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.List;

import cafe.bazar.baharlou.R;
import cafe.bazar.baharlou.customViews.customTextView;
import cafe.bazar.baharlou.localdb.SquareEntity;

public class LocationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<SquareEntity> itemList;
    Context mContext;
    private LayoutInflater mInflater;
    private LocationAdapter.ItemClickListener mClickListener;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public LocationAdapter(Context mContext, List<SquareEntity> itemList) {
        this.mInflater = LayoutInflater.from(mContext);
        this.itemList = itemList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = mInflater.inflate(R.layout.location_adapter, viewGroup, false);
            return new ItemViewHolder(view);
        } else {
            View view = mInflater.inflate(R.layout.item_loading, viewGroup, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ItemViewHolder) {
            SquareEntity item = itemList.get(position);
            ItemViewHolder holderr = (ItemViewHolder) holder;

            // holderr.rowNumber.setText((position + 1) + "");
            holderr.cityText.setText(item.getCity());
            // holderr.stateText.setText(item.getState());
            holderr.nameText.setText(item.getName());

            if (position % 2 > 0)
                holderr.icon.setImageResource(R.drawable.school);
            else holderr.icon.setImageResource(R.drawable.schooll);

        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }

    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed
    }

    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        customTextView cityText, stateText, nameText, rowNumber;
        ImageView icon;

        ItemViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            cityText = itemView.findViewById(R.id.cityText);
            //stateText = itemView.findViewById(R.id.stateText);
            nameText = itemView.findViewById(R.id.nameText);
            //rowNumber = itemView.findViewById(R.id.rowNumber);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemsClick(view, getAdapterPosition());
        }


    }

    // convenience method for getting data at click position
    public SquareEntity getItem(int position) {
        return itemList.get(position);
    }

    // allows clicks events to be caught
    public void setClickListener(LocationAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemsClick(View view, int position);
    }
}
*/
