package places.nearest.tme.views.locationView;

import com.orhanobut.logger.Logger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.CompletableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import places.nearest.tme.apputils.Constants;
import places.nearest.tme.fourSquare.Location;
import places.nearest.tme.fourSquare.SquareModel;
import places.nearest.tme.fourSquare.Venue;
import places.nearest.tme.localdb.DatabaseAdapter;
import places.nearest.tme.localdb.NavigationEntity;
import places.nearest.tme.localdb.SquareEntity;
import places.nearest.tme.retrofit.RetrofitGenerator;
import places.nearest.tme.retrofit.RetrofitInterface;

public class LocationActModel implements LocationActContract.locModel {

    String TAG = LocationActModel.class.getSimpleName();
    LocationActContract.locPresenter presenter;

    DatabaseAdapter adapter = new DatabaseAdapter();

    RetrofitInterface retrofitInterface;
    Disposable disposable;

    @Override
    public void onAttachPresenter(LocationActContract.locPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onGetNearestLocations(String loc) {
        try {

            String date = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());
            String datefSave = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault()).format(new Date());

            retrofitInterface = RetrofitGenerator.createService(RetrofitInterface.class);

            Observable<SquareModel> ObservResult = retrofitInterface.GetNearestLocations(
                    loc,
                    Constants.Square_Client_ID,
                    Constants.Square_Client_Secret,
                    date);

            ObservResult.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<SquareModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            disposable = d;
                            presenter.onDispose(disposable);
                        }

                        @Override
                        public void onNext(SquareModel squareModel) {
                            saveToLocalDB(squareModel, loc, datefSave);
                        }

                        @Override
                        public void onError(Throwable e) {
                            presenter.onGetNearestLocationFailed(e.getLocalizedMessage());
                        }

                        @Override
                        public void onComplete() {
                            Logger.d(TAG , "onComplete");
                        }
                    });


        } catch (Exception ex) {
            presenter.onGetNearestLocationFailed(ex.getLocalizedMessage());
        }

    }

    @Override
    public void onGetCashedLocation() {
        try {
            List<NavigationEntity> lst = adapter.getAll();

            if (lst.size() > 0) {
                NavigationEntity ent = lst.get(lst.size() - 1);
                presenter.onGetCashedSucceed(ent);
            } else
                presenter.onGetCashedSucceed(null);
        } catch (Exception ex) {
            presenter.onGetCashedFailed();
            Logger.e(TAG, ex.getLocalizedMessage());
        }
    }

    //finding cahsed places near the last cashed geo
    @Override
    public void onGetAllData(NavigationEntity ent) {

        List<SquareEntity> finalList = new ArrayList<>();

        try {
            List<SquareEntity> lst = adapter.getAllSquareData();
            if (lst.size() > 0) {
                for (SquareEntity item : lst) {

                    android.location.Location loc1 = new android.location.Location("");
                    loc1.setLatitude(ent.getLatitude());
                    loc1.setLongitude(ent.getLongitude());

                    android.location.Location loc2 = new android.location.Location("");
                    loc2.setLatitude(Double.parseDouble(item.getLat()));
                    loc2.setLongitude(Double.parseDouble(item.getLng()));

                    float distanceInMeters = loc1.distanceTo(loc2);

                    if (distanceInMeters >= Constants.DISTANCE_IN_METER) {
                        finalList.add(item);
                    }
                }

                presenter.onGetAllDataResult(finalList);
            } else {
                presenter.onGetAllDataResult(null);
            }
        } catch (Exception ex) {
            presenter.onGetCashedFailed();
        }
    }

    private void saveToLocalDB(SquareModel squareModel, String loc, String date) {
        try {
            List<SquareEntity> savingList = new ArrayList<>();

            SquareEntity ent = new SquareEntity();
            ent.isMainGeo = true;

            String[] pos = loc.split(",");
            ent.lat = pos[0];
            ent.lng = pos[1];
            ent.date = date;

            savingList.add(ent);

            List<Venue> venuesList = squareModel.getResponse().getVenues();

            for (Venue venue : venuesList) {

                SquareEntity entity = new SquareEntity();
                entity.name = venue.getName();

                Location vLoc = venue.getLocation();
                entity.address = vLoc.getAddress();
                entity.city = vLoc.getCity();
                entity.state = vLoc.getState();
                entity.country = vLoc.getCountry();
                entity.crossStreet = vLoc.getCrossStreet();
                entity.distance = vLoc.getDistance().toString();
                entity.lat = vLoc.getLat().toString();
                entity.lng = vLoc.getLng().toString();

                SquareEntity existedNav = adapter.isExistsInDb(entity);
                if (existedNav != null) {
                    existedNav.distance = vLoc.getDistance().toString();
                    savingList.add(existedNav);
                } else {
                    savingList.add(entity);
                }
            }

            adapter.putSquareData(savingList);
            presenter.onGetNearestLocationSucceed(savingList);

        } catch (Exception ex) {
            Logger.e(TAG, ex.getLocalizedMessage());
        }
    }
}
