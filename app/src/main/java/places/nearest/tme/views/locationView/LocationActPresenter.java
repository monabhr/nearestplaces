package places.nearest.tme.views.locationView;

import java.util.List;


import io.reactivex.disposables.Disposable;
import places.nearest.tme.localdb.NavigationEntity;
import places.nearest.tme.localdb.SquareEntity;

public class LocationActPresenter implements LocationActContract.locPresenter {


    LocationActContract.locView view;
    LocationActContract.locModel model = new LocationActModel();

    public LocationActPresenter(LocationActContract.locView view) {
        this.view = view;
        model.onAttachPresenter(this);
    }

    @Override
    public void onGetNearestLocations(String loc) {
        model.onGetNearestLocations(loc);
    }

    @Override
    public void onGetNearestLocationSucceed(List<SquareEntity> model) {
        view.onGetNearestLocationSucceed(model);
    }

    @Override
    public void onGetNearestLocationFailed(String message) {
        view.onGetNearestLocationFailed(message);
    }

    @Override
    public void onGetCashedLocation() {
        model.onGetCashedLocation();
    }

    @Override
    public void onGetCashedSucceed(NavigationEntity ent) {
        view.onGetCashedSucceed(ent);
    }

    @Override
    public void onGetCashedFailed() {
        view.onGetCashedFailed();
    }

    @Override
    public void onGetAllData(NavigationEntity ent) {
        model.onGetAllData(ent);
    }

    @Override
    public void onGetAllDataResult(List<SquareEntity> model) {
        view.onGetAllDataResult(model);
    }

    @Override
    public void onGetAllDataFailed() {
        view.onGetAllDataFailed();
    }

    @Override
    public void onDispose(Disposable d) {
        view.onDispose(d);
    }
}
