package places.nearest.tme.views.locationView

import android.Manifest
import android.app.ActivityManager
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.*
import com.orhanobut.logger.Logger
import kotlinx.android.synthetic.main.activity_location.*
import android.view.View
import android.view.Menu
import android.widget.Toast
import cafe.bazar.baharlou.views.locationDetailsView.LocationDetailsActivity
import com.foursquare.internal.util.DateTimeUtils
import io.reactivex.disposables.Disposable
import places.nearest.tme.R
import places.nearest.tme.appbase.BaseApplication
import places.nearest.tme.apputils.Constants
import places.nearest.tme.broadcasts.GpsLocationReceiver
import places.nearest.tme.broadcasts.NetworkCheckReceiver
import places.nearest.tme.broadcasts.PhoneShutDownReceiver
import places.nearest.tme.broadcasts.StartUpBootReceiver
import places.nearest.tme.localdb.NavigationEntity
import places.nearest.tme.localdb.PublicMethods
import places.nearest.tme.localdb.PublicMethods.Companion.PersianStringToArabic
import places.nearest.tme.localdb.PublicMethods.Companion.isGpsEnabled
import places.nearest.tme.localdb.SquareEntity
import places.nearest.tme.service.LocationService
import places.nearest.tme.views.locationView.LocationActContract
import places.nearest.tme.views.locationView.LocationActPresenter
import places.nearest.tme.views.locationView.RecyclerViewAdapter
import java.text.SimpleDateFormat
import java.util.*
import java.util.stream.Collectors
import kotlin.collections.ArrayList

class LocationActivity : AppCompatActivity(), LocationActContract.locView,
        SearchView.OnQueryTextListener, RecyclerViewAdapter.OnItemClickListener {


    private var TAG: String = LocationActivity::class.java.simpleName

    val presenter: LocationActContract.locPresenter = LocationActPresenter(this)

    lateinit var locationsAdapter: RecyclerViewAdapter
    lateinit var selectedItem: SquareEntity
    private var lstAr: List<SquareEntity?> = ArrayList<SquareEntity?>()
    private var locationList: List<SquareEntity?> = ArrayList<SquareEntity?>()

    var hasInternet: Boolean = false
    var dateDiff: Long = 0

    var mService: LocationService? = null
    private var mBound = false

    lateinit var font: Typeface

    private var gpsLocationReceiver: GpsLocationReceiver? = null
    private var phoneShutDownReceiver: PhoneShutDownReceiver? = null
    private var networkReceiver: NetworkCheckReceiver? = null
    private var startUpBootReceiver: StartUpBootReceiver? = null
    private var disposable: Disposable? = null

    private var PERMISSION_ALL = 1
    private var PERMISSIONS = arrayOf(
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    )

    /***
     *  notes
     *
     * dateDiff -> diffrence between last cashed data and current date
     * dateDiff > Constants.Date_Diff --> if diffrence is greater than 48 hours
     *
     * Constants.Page --> for RecyclerView loadMore
     */


//**************************************************************************//

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        if (!checkPermissions()) {
            requestPermissions()
        }

        //doBindService()

        //setting font face
        font = BaseApplication.typeFace

        registerBroadcasts()

        setToolBar()

        /**check if some conditions occured or not
         *  then --> loading data from SquareApi or getting them form localDb
         */
        loadCashedData()

        /* //just for my test
         for (i in 0 until Constants.Page) {
             var sq: SquareEntity = SquareEntity()
             sq.setCity("city" + i)
             sq.setName("name" + i + " family ")
             lstAr.add(sq)
         }
         setLocationAdapter(lstAr)*/


    }

    private fun loadCashedData() {
        presenter.onGetCashedLocation()
    }

    override fun onGetCashedSucceed(ent: NavigationEntity?) {

        //if internet is not connected OR diffrence between last cashed date
        // and current date is not greater than 48 hours
        //then load cashed location ELSE get data from SquareApi

        hasInternet = PublicMethods.isConnectedToInternet()

        if (ent != null) {
            dateDiff = PublicMethods.DateDiff(ent.navDate)

            if (!hasInternet || dateDiff < Constants.Date_Diff) {
                presenter.onGetAllData(ent)
            } else {
                doBindService()
            }
        } else {

            doBindService()
        }
    }

    override fun onGetCashedFailed() {
        doBindService()
    }


    override fun onGetAllDataResult(model: MutableList<SquareEntity>?) {

        //cashed data is null OR not
        if (model != null) {
            lstAr = ArrayList<SquareEntity?>()
            locationList = model

            //if size of the data is greater than 10 then load first 10 data
            //else load all of the data
            if (locationList.size <= Constants.Page) {
                for (i in 0 until Constants.Page) {
                    (lstAr as ArrayList<SquareEntity?>).add(locationList[i])
                }
            } else {
                lstAr = ArrayList(locationList)
            }

            setLocationAdapter(lstAr, locationList)

        } else {
            PublicMethods.toast(getString(R.string.no_places_found))
        }
    }

    override fun onGetAllDataFailed() {

    }

    private fun setToolBar() {

        setSupportActionBar(toolbar)

        /* actionBar = supportActionBar!!

         setActionBarTitle()

         actionBar.setDisplayHomeAsUpEnabled(true)

         actionBar.setHomeAsUpIndicator(
                 IconDrawable(this, FontAwesomeIcons.fa_bars)
                         .colorRes(R.color.colorAccent)
                         .actionBarSize())

         actionBar.setHomeButtonEnabled(true)*/
    }

    override fun onQueryTextChange(query: String?): Boolean {

        //implementing search on RecyclerView
        if (query != null) {
            var filtered: List<SquareEntity> = filter(locationList, query!!)
            lstAr = ArrayList()

            try {
                if (filtered.isNotEmpty()) {
                    if (filtered.size <= Constants.Page) {
                        for (i in 0 until filtered.size) {
                            (lstAr as ArrayList<SquareEntity?>).add(filtered[i])
                        }
                    } else {
                        for (i in 0 until Constants.Page) {
                            (lstAr as ArrayList<SquareEntity?>).add(filtered[i])
                        }
                    }
                } else {
                    lstAr = ArrayList(filtered)
                }
            } catch (ex: Exception) {
                Logger.i(TAG, ex.localizedMessage)
            }

            setLocationAdapter(lstAr, filtered)
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val searchItem = menu!!.findItem(R.id.action_search)
        val searchView: SearchView = searchItem.actionView as SearchView
        //val searchView = MenuItemCompat.getActionView(searchItem) as SearchView
        searchView.setOnQueryTextListener(this)

        return true
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    private fun filter(models: List<SquareEntity?>, query: String): List<SquareEntity> {

        //searching in list
        val lowerCaseQuery = query.toLowerCase()

        val filteredModelList = java.util.ArrayList<SquareEntity>()
        for (model in models!!) {
            try {
                val text = PersianStringToArabic(model!!.getName()!!.toLowerCase())
                if (text.contains(lowerCaseQuery)) {
                    filteredModelList.add(model)
                }
            } catch (ex: Exception) {
                Logger.i(TAG, ex.localizedMessage)
            }
        }
        return filteredModelList
    }

    private fun registerBroadcasts() {

        LocalBroadcastManager.getInstance(this).registerReceiver(locationReceiver,
                IntentFilter(Constants.ACTION_BROADCAST))

        val mIntentFilter = IntentFilter()
        mIntentFilter.addAction(LocationManager.PROVIDERS_CHANGED_ACTION)
        gpsLocationReceiver = GpsLocationReceiver()
        registerReceiver(gpsLocationReceiver, mIntentFilter)

        val netFilter = IntentFilter()
        netFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        networkReceiver = NetworkCheckReceiver()
        registerReceiver(networkReceiver, netFilter)

        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_SHUTDOWN)
        intentFilter.addAction(Intent.ACTION_REBOOT)

        phoneShutDownReceiver = PhoneShutDownReceiver()
        registerReceiver(phoneShutDownReceiver, intentFilter)

        val bootIntentFilter = IntentFilter()
        bootIntentFilter.addAction(Intent.ACTION_BOOT_COMPLETED)
        bootIntentFilter.addAction("android.intent.action.QUICKBOOT_POWERON")

        startUpBootReceiver = StartUpBootReceiver()
        registerReceiver(startUpBootReceiver, bootIntentFilter)

    }

    private var mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val binder = service as LocationService.LocalBinder
            mService = binder.service

            if (mService != null) {
                Logger.i("service-bind Service is bonded successfully!")
                if (!checkPermissions()) {
                    requestPermissions()
                }


                if (hasInternet || dateDiff > Constants.Date_Diff)
                    mService!!.requestLocationUpdates()
            } else {
                Logger.i("service-bind Service is not bonded !")
            }

            mBound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            mService = null
            mBound = false
        }
    }

    private fun requestPermissions() {

        val shouldProvideRationale = (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION))
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                ||
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.READ_EXTERNAL_STORAGE)

        if (shouldProvideRationale) {
            try {
                Logger.i(TAG, "Displaying permission  to provide additional context.")
                if (activity_main_c != null) {
                    Snackbar.make(
                            (activity_main_c!!),
                            R.string.gps_permission,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction("Ok") {
                                ActivityCompat.requestPermissions(this,
                                        PERMISSIONS, PERMISSION_ALL)
                            }.show()
                }
            } catch (ex: Exception) {
                Logger.e(TAG + ex.localizedMessage);
            }

        } else {
            Logger.i(TAG, "Requesting permission")
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL)
        }
    }

    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                &&
                PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                &&
                PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                &&
                PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)

    }

    private fun doBindService() {

        if (!checkPermissions()) {
            requestPermissions()
        }

        if (!hasInternet || !isGpsEnabled()) {
            Handler().postDelayed({
                PublicMethods.toast(getString(R.string.no_places_found))
            }, 4000)
        }

        if (mService != null) {
            mService!!.requestLocationUpdates()

        } else {
            val intent = Intent(this, LocationService::class.java)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ContextCompat.startForegroundService(this, intent)
            } else {
                this.startService(intent)
            }

            bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE)

            mBound = true
        }
    }

    private fun onStopService() {
        try {
            if (mBound) {
                mService!!.removeLocationUpdates()
                unbindService(mServiceConnection)
                mBound = false
            }

            val intent = Intent(this, LocationService::class.java)
            stopService(intent)

            unregisterBroadcasts()

        } catch (ex: Exception) {
            Logger.e(TAG, ex.localizedMessage)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        onStopService()
    }

    private fun unregisterBroadcasts() {
        // Unregister  receivers
        if (gpsLocationReceiver != null) {
            unregisterReceiver(gpsLocationReceiver)
        }

        if (phoneShutDownReceiver != null) {
            unregisterReceiver(phoneShutDownReceiver)
        }

        if (startUpBootReceiver != null) {
            unregisterReceiver(startUpBootReceiver)
        }

        if (networkReceiver != null) {
            unregisterReceiver(networkReceiver)
        }
    }

    private val locationReceiver = object : BroadcastReceiver() {

        //getting new location from locationService and save it
        override fun onReceive(context: Context, intent: Intent) {
            when {
                intent.hasExtra(Constants.EXTRA_LOCATION) -> {

                    // if (count == 0) {//just for my test
                    progressBar.visibility = View.VISIBLE

                    val loc: Location = intent.getParcelableExtra(Constants.EXTRA_LOCATION)

                    val locString = loc.latitude.toString() + "," + loc.longitude.toString()
                    //PublicMethods.toast("intent received " + locString)

                    presenter.onGetNearestLocations(locString)
                    Logger.i(TAG + " " + loc)


                }
            }
        }
    }

    override fun onGetNearestLocationSucceed(model: List<SquareEntity>?) {

        //result of SquareApi

        locationList = model!!
        lstAr = ArrayList()

        if (locationList.size <= Constants.Page) {
            for (i in 0 until Constants.Page) {
                (lstAr as ArrayList<SquareEntity?>).add(locationList[i])
            }
        } else {
            lstAr = ArrayList(locationList)
        }

        progressBar.visibility = View.GONE

        setLocationAdapter(lstAr, locationList)

    }

    override fun onGetNearestLocationFailed(message: String?) {
        try {

            progressBar.visibility = View.GONE
            PublicMethods.toast(message!!)

        } catch (ex: Exception) {
            Logger.e(TAG, ex.localizedMessage)
        }
    }

    private fun setLocationAdapter(arr: List<SquareEntity?>, mainList: List<SquareEntity?>) {
        loc_recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        locationsAdapter = RecyclerViewAdapter(this, arr, loc_recyclerView, locationList.size)
        locationsAdapter.setOnItemListener(this)
        loc_recyclerView.adapter = locationsAdapter

        //set load more listener for the RecyclerView adapter
        locationsAdapter.setOnLoadMoreListener(
                object : RecyclerViewAdapter.OnLoadMoreListener {
                    override fun onLoadMore() {
                        if (lstAr.size <= mainList.size - 1) {

                            (lstAr as ArrayList<SquareEntity?>).add(null)

                            locationsAdapter.notifyItemInserted(lstAr.size - 1)

                            Handler().postDelayed({
                                (lstAr as ArrayList<SquareEntity?>).removeAt(lstAr.size - 1)
                                locationsAdapter.notifyItemRemoved(lstAr.size)

                                //Generating more data
                                var index: Int = lstAr.size
                                var end: Int = index + Constants.Page

                                for (i in 0 until end) {
                                    try {
                                        if (end < mainList.size)
                                            (lstAr as ArrayList<SquareEntity?>).add(mainList[i])
                                    } catch (ex: Exception) {

                                    }
                                }

                                /* for (i in 0 until end) { //just for my test
                                     var sq: SquareEntity = SquareEntity()
                                     sq.setCity("city" + i)
                                     sq.setName("name" + i + " family ")
                                     lstAr.add(sq)
                                 }*/


                                locationsAdapter.notifyDataSetChanged()
                                locationsAdapter.setLoaded()
                            }, 3000)
                        } else {
                            PublicMethods.toast("Loading data completed")
                        }
                    }
                })

    }

    override fun onDispose(d: Disposable?) {
        disposable = d
    }


    override fun onItemClick(item: SquareEntity?) {
        //occured if clicked on recyclerView Items

        if (item != null) {
            selectedItem = item
            val intent = Intent(this, LocationDetailsActivity::class.java)
            intent.putExtra(Constants.Detail, item)
            startActivity(intent)
            (this as AppCompatActivity).overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        }
    }

}
