package places.nearest.tme.localdb;

import android.location.Location;

import java.util.List;

import io.objectbox.BoxStore;
import places.nearest.tme.appbase.BaseApplication;

public class DatabaseAdapter {

    private static final String TAG = DatabaseAdapter.class.getSimpleName();
    private final BoxStore boxStore;


    public DatabaseAdapter() {
        boxStore = BaseApplication.getApp().getBoxStore();
    }

    public void clearDatabase() {
        boxStore.boxFor(NavigationEntity.class).removeAll();
    }

    public long insertLocation(NavigationEntity model) {
        return boxStore.boxFor(NavigationEntity.class).put(model);
    }

    public List<NavigationEntity> getAll() {
        return boxStore.boxFor(NavigationEntity.class).getAll();
    }

    public List<NavigationEntity> getUnsendList() {
        return boxStore.boxFor(NavigationEntity.class).query()
                .equal(NavigationEntity_.isSend, false).build().find();
    }

    public long putSquareData(SquareEntity model) {
        return boxStore.boxFor(SquareEntity.class).put(model);
    }

    public void putSquareData(List<SquareEntity> model) {
        boxStore.boxFor(SquareEntity.class).put(model);
    }

    public List<SquareEntity> getAllSquareData() {
        return boxStore.boxFor(SquareEntity.class).getAll();
    }

    public SquareEntity isExistsInDb(SquareEntity ent) {
        return boxStore.boxFor(SquareEntity.class).query()
                .equal(SquareEntity_.lat, ent.lat)
                .equal(SquareEntity_.lng, ent.lng)
                .build().findFirst();
    }


}
