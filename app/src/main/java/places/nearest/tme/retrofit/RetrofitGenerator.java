package places.nearest.tme.retrofit;

import com.orhanobut.logger.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import places.nearest.tme.apputils.Constants;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitGenerator {

    private static String token = null;

    public static <S> S createService(Class<S> serviceClass) {

        String BASE_URL = Constants.SQUARE_ADDRESS;

        HttpLoggingInterceptor loggerInterceptorBody = new HttpLoggingInterceptor();
        loggerInterceptorBody.setLevel(HttpLoggingInterceptor.Level.BODY);

        HttpLoggingInterceptor loggerInterceptorHeader = new HttpLoggingInterceptor();
        loggerInterceptorHeader.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpclient = new OkHttpClient.Builder();
        httpclient.addInterceptor(chain -> {
            Request original = chain.request();

            Request request = original.newBuilder()
                    //.header("Connection", "close")
                    //.header("Content-Encoding", "gzip, deflate")
                    .header("Content-Type", "application/json;charset=utf-8")
                    .header("Accept", "application/json")
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpclient.addInterceptor(logging);
        }

        OkHttpClient client = httpclient
                .addInterceptor(loggerInterceptorBody)
                .addInterceptor(loggerInterceptorHeader)
                .readTimeout(30L, TimeUnit.SECONDS)
                .connectTimeout(30L, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = (new Retrofit.Builder())
                .baseUrl(BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                //.addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).
                        build();

        return retrofit.create(serviceClass);


    }
}
