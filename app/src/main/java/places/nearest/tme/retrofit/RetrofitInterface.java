package places.nearest.tme.retrofit;

import io.reactivex.Observable;
import places.nearest.tme.fourSquare.SquareModel;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterface {

    @GET("/v2/venues/search")
    Observable<SquareModel> GetNearestLocations(@Query("ll") String ll,
                                                @Query("client_id") String client_id,
                                                @Query("client_secret") String client_secret,
                                                @Query("v") String v);


}
