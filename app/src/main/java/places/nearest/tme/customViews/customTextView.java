package places.nearest.tme.customViews;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import places.nearest.tme.appbase.BaseApplication;


public class customTextView extends AppCompatTextView {

    public customTextView(Context context) {
        super(context);

        this.setTypeface(BaseApplication.typeFace);
    }

    public customTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface(BaseApplication.typeFace);

    }


}
