package places.nearest.tme.roomdb;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import places.nearest.tme.classes.NavigationEnt;
import places.nearest.tme.dao.NavigationEntDAO;

@Database(entities = NavigationEnt.class, version = 1)
public abstract class RoomDB extends RoomDatabase {

    private static final String DB_NAME = "nearest_database";

    public abstract NavigationEntDAO navDao();

    private static volatile RoomDB INSTANCE;

    public static RoomDB getInstance(final Context context) {

        if (INSTANCE == null) {
            synchronized (RoomDB.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDB.class, DB_NAME)
                            .build();
                }

            }
        }
        return INSTANCE;
    }
}
