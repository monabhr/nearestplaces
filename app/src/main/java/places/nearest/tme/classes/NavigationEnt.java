package places.nearest.tme.classes;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "NavigationEnt")
public class NavigationEnt {

    @PrimaryKey
    public Long id;

    @NonNull
    @ColumnInfo(name = "latitude")
    public double latitude;

    @NonNull
    @ColumnInfo(name = "longitude")
    private double longitude;

    public String label;
    public float speed;
    public float accuracy;
    public float bearing;
    public String description;
    public String navDate;
    public int confidence;
    public double altitude;
    public boolean isSend;


    public NavigationEnt() {
    }

    public NavigationEnt(double latitude, double longitude, String label, float speed, float accuracy, float bearing, String description, String navDate, int confidence, double altitude, boolean isSend) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.label = label;
        this.speed = speed;
        this.accuracy = accuracy;
        this.bearing = bearing;
        this.description = description;
        this.navDate = navDate;
        this.confidence = confidence;
        this.altitude = altitude;
        this.isSend = isSend;
    }
}
