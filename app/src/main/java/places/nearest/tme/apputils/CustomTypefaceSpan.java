package places.nearest.tme.apputils;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

public class CustomTypefaceSpan extends TypefaceSpan {

    private final Typeface newType;
    private final float textSize_;
    private final int color_;

    public CustomTypefaceSpan(String family, Typeface type, float textSize, int color) {
        super(family);
        newType = type;
        textSize_ = textSize;
        color_ = color;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, newType, textSize_, color_);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, newType, textSize_, color_);
    }

    private static void applyCustomTypeFace(Paint paint, Typeface tf, float textSize, int colr) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(false);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        if (colr != 0)
            paint.setColor(colr);

        paint.setTextSize(textSize);

        paint.setTypeface(tf);
    }
}