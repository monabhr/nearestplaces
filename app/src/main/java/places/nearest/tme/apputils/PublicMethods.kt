package places.nearest.tme.localdb

import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager
import android.view.Gravity
import android.widget.Toast
import com.orhanobut.logger.Logger
import places.nearest.tme.appbase.BaseApplication
import java.text.SimpleDateFormat
import java.util.*

class PublicMethods {

    companion object {

        private val ArabicYeChar = 1610.toChar()
        private val PersianYeChar = 1740.toChar()

        private val ArabicKeChar = 1603.toChar()
        private val PersianKeChar = 1705.toChar()

        fun toast(msg: String) {
            var toast: Toast = Toast.makeText(BaseApplication.getApp(), msg, Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0)
            toast.show()
        }


        fun isConnectedToInternet(): Boolean {

            val ctx = BaseApplication.getApp()

            val connectivityManager = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected

        }

        fun isGpsEnabled(): Boolean {

            var gps_enabled = false
            var network_enabled = false

            val lm = BaseApplication.getApp()
                    .getSystemService(Context.LOCATION_SERVICE) as LocationManager

            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            return gps_enabled || network_enabled

        }

        fun PersianStringToArabic(value: String): String {
            return value.replace(PersianYeChar, ArabicYeChar)

        }

        fun DateDiff(date: String): Long {

            var simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")

            try {
                var date1: Date = simpleDateFormat.parse(date)
                val c = Calendar.getInstance().time

                var date2: Date = simpleDateFormat.parse(simpleDateFormat.format(c))

                var diff: Long = date1.time - date2.time

                var seconds = diff / 1000
                var minutes = seconds / 60
                var hours = minutes / 60
                var days = hours / 24

                return days

            } catch (e: Exception) {
                Logger.e(e.localizedMessage)
            }

            return 0

        }


    }

}