package places.nearest.tme.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import places.nearest.tme.classes.NavigationEnt;

@Dao
public interface NavigationEntDAO {


    @Query("SELECT * FROM NavigationEnt")
    List getAllRepos();

    @Insert
    void insert(NavigationEnt... repos);

    @Update
    void update(NavigationEnt... repos);

    @Delete
    void delete(NavigationEnt... repos);

}
