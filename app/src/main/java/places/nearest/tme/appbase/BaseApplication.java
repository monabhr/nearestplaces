package places.nearest.tme.appbase;

import android.app.Application;
import android.graphics.Typeface;

import com.crashlytics.android.Crashlytics;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import io.fabric.sdk.android.Fabric;
import io.objectbox.BoxStore;
import places.nearest.tme.apputils.Constants;
import places.nearest.tme.localdb.MyObjectBox;

public class BaseApplication extends Application {

    private static BaseApplication baseApp;
    public static Typeface typeFace;
    private static BoxStore boxStore;

    @Override
    public void onCreate() {
        super.onCreate();

        baseApp = this;

        typeFace = Typeface.createFromAsset(getAssets(), Constants.appFontName);

        boxStore = MyObjectBox.builder().androidContext(baseApp).build();

        Fabric.with(this, new Crashlytics());
        //Iconify.with(new FontAwesomeModule());
        //Hawk.init(this).build();

        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return true;//BuildConfig.DEBUG;
            }
        });

    }


    public static BaseApplication getApp() {
        return baseApp;
    }

    public BoxStore getBoxStore() {
        return boxStore;
    }
}
